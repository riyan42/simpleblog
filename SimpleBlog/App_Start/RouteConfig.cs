﻿using SimpleBlog.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SimpleBlog
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var namespacesX = new[] { typeof(PostsController).Namespace };
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("TagForRealThisTime", "tag/{idAndSlug}", new { controller="Posts",action="Tag"},namespacesX);
            routes.MapRoute("Tag", "tag/{id}-{slug}",new {controller="Posts",action="Tag" },namespacesX);

            // -http://blog.dev/post/432-this-is-a-slug
            routes.MapRoute("PostForRealThisTime", "post/{idAndSlug}", new { controller="Posts",action="Show"},namespacesX);
            routes.MapRoute("Post", "post/{id}-{slug}", new { controller = "Posts", action = "Show" }, namespacesX);

            routes.MapRoute("Login", "login", new { controller = "Auth", action = "login" }, namespacesX);
            routes.MapRoute("Logout", "logout", new { controller = "Auth", action = "Logout" }, namespacesX);

            //default route ditandai dengan url="" seperti dibawah
            routes.MapRoute("Home", "", new { controller = "Posts", action = "Index" }, namespacesX);

            routes.MapRoute("Sidebar", "", new { controller = "Layout", action = "sidebar" }, namespacesX);
            
            //atau  bisa dengan ini karna defaultnya -http://localhost/Posts/Index/1 -http://localhost/Posts/Index/2 dan seterusnya
            //routes.MapRoute(
            //    name: "Home",
            //    url: "{controller}/{action}/{page}",
            //    defaults: new { controller = "Posts", action = "Index", page = UrlParameter.Optional },
            //    namespaces: namespacesX
            //);

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);
        }
    }
}