﻿
using SimpleBlog.ViewModels;
using System.Web.Mvc;
using System.Web.Security;
using NHibernate.Linq;
using System.Linq;
using SimpleBlog.Models;
namespace SimpleBlog.Controllers
{
    public class AuthController : Controller
    {
        public ActionResult Logout() {
            FormsAuthentication.SignOut();

            return RedirectToRoute("home");
        }

        public ActionResult login()
        {
            return View(new AuthLogin {           
            });
        }

        [HttpPost]
        public ActionResult login(AuthLogin form,string returnUrl)
        {
            

            var user = Database.Session.Query<User>().FirstOrDefault(u=>u.Username==form.Username);

            if (user == null) {
                SimpleBlog.Models.User.FakeHash();

                ModelState.AddModelError("Username", "Username is incorrect");
                return View(form);
            }


            if (!user.CheckPassword(form.Password))
            {
                ModelState.AddModelError("Password", "Password is incorrect");
                return View(form);
            }

            if (!ModelState.IsValid) return View(form);
            
            FormsAuthentication.SetAuthCookie(user.Username,true);
            if(!string.IsNullOrWhiteSpace(returnUrl))return Redirect(returnUrl);
            
            return RedirectToRoute("home");            
        }
	}
}