﻿using NHibernate.Linq;
using System.Linq;
using SimpleBlog.Models;
using SimpleBlog.ViewModels;
using System.Web.Mvc;

namespace SimpleBlog.Controllers
{
    public class LayoutController : Controller
    {
        [ChildActionOnly]
        public ActionResult Sidebar()
        {
            var layoutsidebar = new LayoutSidebar
            {
                IsLoggedIn = Auth.User != null,
                UserName = Auth.User != null ? Auth.User.Username : "",
                IsAdmin = User.IsInRole("admin"),

                Tags = Database.Session.Query<Tag>().Select(tag => new
                {
                    tag.Id,
                    tag.Name,
                    tag.Slug,
                    PostCount = tag.Posts.Count()
                }).Where(t => t.PostCount > 0).OrderByDescending(t => t.PostCount).Select(
                    tag => new SideBarTag(tag.Id, tag.Name, tag.Slug, tag.PostCount)).ToList()
            };

            return View(layoutsidebar);
        }
	}
}