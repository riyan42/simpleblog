﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace SimpleBlog.Infrastructure
{
    public class PagedData<T>:IEnumerable<T>
    {
        private readonly IEnumerable<T> _currentItems;
        public int TotalCount { private set; get; }
        public int Page {private set; get; }
        public int PerPage {private  set; get; }
        public int TotalPages { private set; get; }
        public bool HasNextPage { private set; get; }
        public bool HasPreviousPage { private set; get; }

        public int NextPage 
        {
            get{
                if (!HasNextPage)
                    throw new InvalidOperationException();

                return Page + 1;
            }
        }

        public int PreviousPage
        {
            get
            {
                if (!HasPreviousPage)
                    throw new InvalidOperationException();

                return Page - 1;
            }
        }

        public PagedData(IEnumerable<T> currentItems,int totalCount,int page,int perPage) 
        {
            _currentItems = currentItems;
            TotalCount = totalCount;
            Page=page;
            PerPage = perPage;

            TotalPages = (int)Math.Ceiling((float)TotalCount / PerPage);
            HasNextPage = Page < TotalPages;
            HasPreviousPage = Page > 1;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _currentItems.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}