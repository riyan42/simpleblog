﻿$(document).ready(function () {
    $("[data-post]").click(function (e) {
        e.preventDefault();
        var $this = $(this);
        var message = $this.data("post");    


        if (message != "") {//melalui Konfirmasi terlebih dahulu
            bootbox.dialog({
                message: message,
                title: "<b>Konfirmasi!</b>",
                onEscape: function () { false },
                buttons: {
                    danger: { label: "Batal", className: "btn-danger", callback: function () { false; } },
                    success: {
                        label: "Oke",
                        className: "btn-primary",
                        callback: function () {
                            //langsung melakukan action post
                            var antiForgeryToken = $("#anti-forgery-form input");
                            var antiForgeryInput = $("<input type='hidden'>").attr("name", antiForgeryToken.attr("name")).val(antiForgeryToken.val());
                            $("<form>").attr("method", "post").attr("action", $this.attr("href")).append(antiForgeryInput).appendTo(document.body).submit();
                        }
                    }
                }
            });
        } else {//kalau tanpa konfirmasi langsung melakukan aksi
            var antiForgeryToken = $("#anti-forgery-form input");
            var antiForgeryInput = $("<input type='hidden'>").attr("name", antiForgeryToken.attr("name")).val(antiForgeryToken.val());
            $("<form>").attr("method", "post").attr("action", $this.attr("href")).append(antiForgeryInput).appendTo(document.body).submit();
        }        
    
        //if (message !="") {//melalui Konfirmasi terlebih dahulu
        //    bootbox.confirm(message, function (result) {
        //        if (result == false)
        //            return;
        //        //langsung melakukan action post
        //        var antiForgeryToken = $("#anti-forgery-form input");
        //        var antiForgeryInput = $("<input type='hidden'>").attr("name", antiForgeryToken.attr("name")).val(antiForgeryToken.val());

        //        $("<form>")
        //            .attr("method", "post")
        //            .attr("action", $this.attr("href"))
        //            .append(antiForgeryInput)
        //            .appendTo(document.body)
        //            .submit();
        //    });
        //} else {//kalau tanpa konfirmasi langsung melakukan aksi
        //    var antiForgeryToken = $("#anti-forgery-form input");
        //    var antiForgeryInput = $("<input type='hidden'>").attr("name", antiForgeryToken.attr("name")).val(antiForgeryToken.val());

        //    $("<form>")
        //        .attr("method", "post")
        //        .attr("action", $this.attr("href"))
        //        .append(antiForgeryInput)
        //        .appendTo(document.body)
        //        .submit();
        //}        
        //if (message && !confirm("apakah anda yakin")) return;

        //var antiForgeryToken = $("#anti-forgery-form input");
        //var antiForgeryInput = $("<input type='hidden'>").attr("name", antiForgeryToken.attr("name")).val(antiForgeryToken.val());

        //$("<form>")
        //    .attr("method", "post")
        //    .attr("action", $this.attr("href"))
        //    .append(antiForgeryInput)
        //    .appendTo(document.body)
        //    .submit();
    });

    $("[data-slug]").each(function() {
        var $this = $(this);
        var $sendSlugFrom = $($this.data("slug"));

        $sendSlugFrom.keyup(function() {
            var slug = $sendSlugFrom.val();
            slug = slug.replace(/[^a-zA-Z0-9\s]/g, "");
            slug = slug.toLowerCase();
            slug = slug.replace(/\s+/g, "-");

            if (slug.charAt(slug.length - 1) == "-")
                slug = slug.substr(0, slug.length - 1);

            $this.val(slug);
        });
    });

    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 6000);

});