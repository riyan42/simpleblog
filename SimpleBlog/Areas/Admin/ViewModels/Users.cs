﻿using System.Collections.Generic;
using SimpleBlog.Models;
using System.ComponentModel.DataAnnotations;

namespace SimpleBlog.Areas.Admin.ViewModels
{
    public class RoleCheckbox 
    {
        public int Id { get; set; }
        public bool IsChecked { set; get; }
        public string Name { set; get; }
    }

    public class UsersIndex
    {
        public IEnumerable<User> Users { set; get; }

    }
    public class UserNew
    {
        public IList<RoleCheckbox> Roles { set; get; }

        [Required,MaxLength(128)]
        public string Username{ get; set;}

        [Required,DataType(DataType.Password)]
        public string Password { get; set; }

         [Required,MaxLength(245), DataType(DataType.EmailAddress)]
        public string Email { get; set; }

    }
    public class UserEdit{
        public IList<RoleCheckbox> Roles { set; get; }

        [Required, MaxLength(128)]
        public string Username { get; set; }

        [Required, MaxLength(245), DataType(DataType.EmailAddress)]
        public string Email { get; set; }        
    }
    public class UserResetPasswword
    {
        public string Username { get; set; }

        [Required, DataType(DataType.Password)]
        public string Password { get; set; }
    }


}