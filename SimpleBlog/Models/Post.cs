﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
namespace SimpleBlog.Models
{
    public class Post
    { 
        public virtual int Id { set; get; }
        public virtual User User { set; get; }
        public virtual string Title { set; get; }
        public virtual string Slug { set; get; }
        public virtual string Content { set; get; }

        public virtual DateTime CreatedAt { set; get; }
        public virtual DateTime? UpdatedAt { set; get; }
        public virtual DateTime? DeletedAt { set; get; }
        public virtual bool IsDeleted { get { return DeletedAt != null; } }

        public virtual IList<Tag> Tags { set; get; }
        public Post()
        {
            Tags = new List<Tag>();
        }
    }

    public class PostMap : ClassMapping<Post> 
    {
        public PostMap() 
        {
            Table("posts");
            
            Id(x => x.Id, x => x.Generator(Generators.Identity));
            
            ManyToOne(x => x.User, x => 
            {
                x.Column("user_id");
                x.NotNullable(true);
            });

            Property(x => x.Title, x => x.NotNullable(true));
            Property(x => x.Slug, x => x.NotNullable(true));
            Property(x => x.Content, x => x.NotNullable(true));
            Property(x => x.CreatedAt, x =>
            {
                x.Column("created_at");
                x.NotNullable(true);
            });
            Property(x => x.UpdatedAt, x => x.Column("updated_at"));
            Property(x => x.DeletedAt, x => x.Column("deleted_at"));

            Bag(x => x.Tags, x => {
                x.Key(y=>y.Column("post_id"));
                x.Table("post_tags");
            },x=>x.ManyToMany(y=>y.Column("tag_id")));

        }
    }
}